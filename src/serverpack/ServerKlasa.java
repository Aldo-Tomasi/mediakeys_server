package serverpack;


import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.*;

import commands.MediaKeys;


public class ServerKlasa implements Runnable{

	private ServerSocket serverSock;
	private Socket sock = null;
	private ObjectInputStream objInputStr;
	private ObjectOutputStream objOutputStr;
	private String message="";
	
	public ServerKlasa(int port) throws IOException {
		
		serverSock = new ServerSocket(port);
		//serverSock.setSoTimeout(10000);
	}
	
	
	@Override
	public void run() {
		
		listenAndSetup();
		
		while(true) {
			
			try {
				
				// if(sock.getInetAddress().equals(null)) {System.out.println("socket je null"); clean(); listenAndSetup();}
				message = objInputStr.readObject().toString();
				System.out.println("MESSAGE: " + message);
				sendThis(message);
					
					
			} catch (EOFException EOFe){
				System.out.println (EOFe.getMessage());
				clean();
				listenAndSetup();
			} catch (SocketException se){
				System.out.println (se.getMessage());
				clean();
				listenAndSetup();
				
			} catch (ClassNotFoundException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	private void listenAndSetup() {
		
		try {
			
			System.out.println("Waiting for client at port "+serverSock.getLocalPort());
			sock = serverSock.accept();
			System.out.println("Connected to "+sock.getRemoteSocketAddress());

			objInputStr = new ObjectInputStream(sock.getInputStream());
			
			objOutputStr = new ObjectOutputStream(sock.getOutputStream());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("Error: "+e.getMessage());
		}
		
	}
	
	private void clean() {
		
		try {
			if(objOutputStr.equals(null)) {objOutputStr.close();}
			if(objInputStr.equals(null)) {objInputStr.close();}
			if(!sock.isClosed()) {sock.close();}
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private void sendThis(String code) {
		switch(code) {
		case "1": MediaKeys.songPlayPause(); break;
		case "2": MediaKeys.songNext();break;
		case "3": MediaKeys.songPrevious();break;
		case "4": MediaKeys.volumeUp();break;
		case "5": MediaKeys.volumeDown();break;
		case "6": MediaKeys.volumeMute();break;
		
		}
	}
	
	public String getLocalIP() throws UnknownHostException {
		
        InetAddress inetAddress = InetAddress.getLocalHost(); // Unreliable! Will be problematic if server has multiple connections.
		return inetAddress.getHostAddress();
        
	}
	
}
	
	
	
