package maintenanceController;

import java.io.File;
import java.io.IOException;
import java.lang.String;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.*;

public class FileController {
	
	private static final String FILENAME = "portInfo.txt";
	private static final String DEFAULT_PORT = "1906";
	
	public void saveToFile(String input) {
		
		File file = new File(FILENAME);
		try {
			
			FileUtils.writeStringToFile(file, input, "UTF-8", false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String readFromFile() {
		
		File file = new File(FILENAME);
		
		String output = null;
		
		try {
			output = FileUtils.readFileToString(file,"UTF-8");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (output==null || StringUtils.isBlank(output) || output.isEmpty()) {output="1906";}
		return output;
		
	}
	
	public void writeDefaultFile() {
		
		File file = new File(FILENAME);
		
		try {
			FileUtils.writeStringToFile(file, DEFAULT_PORT, "UTF-8", false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean fileExists() {
		
		File file = new File(FILENAME);
		
		if(file.exists()) {return true;}
		else return false;
		
		
	}
	
}
