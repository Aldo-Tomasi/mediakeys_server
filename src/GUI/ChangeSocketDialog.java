// IMPORTANT. This is useless for now, as on client side I don't have change port functionality 

package GUI;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import maintenanceController.FileController;

import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JLabel;

public class ChangeSocketDialog extends JDialog {
	
	FileController fileControl = new FileController();

	private final JPanel contentPanel = new JPanel();
	private JTextField portInputField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ChangeSocketDialog dialog = new ChangeSocketDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public ChangeSocketDialog() {
		setBounds(100, 100, 450, 100);
		getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		{
			JLabel lblPort = new JLabel("Enter port number here: ");
			getContentPane().add(lblPort);
		}
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		{
			portInputField = new JTextField();
			portInputField.addKeyListener(new KeyAdapter() {
				@Override
				public void keyTyped(KeyEvent evt) {
					
					char c = evt.getKeyChar();
					if(!(Character.isDigit(c)) || (c==KeyEvent.VK_DELETE) || (c==KeyEvent.VK_BACK_SPACE)){
							evt.consume();
							}
				}
			});
			portInputField.setToolTipText("enterPortHere(DEFAULT: 1906)");
			contentPanel.add(portInputField);
			portInputField.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						fileControl.saveToFile(portInputField.getText().toString());
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
