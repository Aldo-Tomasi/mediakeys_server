package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import java.awt.SystemColor;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

import serverpack.ServerKlasa;

import maintenanceController.FileController;


public class RemoteKeysServer {

	private JFrame frmRemotekeysV;
	private JTextField ipAddrTxt;
	private JTextField portTxt;
	private static String ipAddr;
	private static String port;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		FileController fileControl = new FileController();

		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					if(!fileControl.fileExists()) {
						fileControl.writeDefaultFile();
						}
					
					ServerKlasa serverklasa = new ServerKlasa(Integer.parseInt(fileControl.readFromFile()));
					ipAddr = serverklasa.getLocalIP();
					port = fileControl.readFromFile();
					
					RemoteKeysServer window = new RemoteKeysServer();
					
					Thread t1 = new Thread(serverklasa);
					t1.start();
					
					window.frmRemotekeysV.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RemoteKeysServer() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmRemotekeysV = new JFrame();
		frmRemotekeysV.setTitle("RemoteKeys v0.2");
		frmRemotekeysV.setBounds(100, 100, 500, 332);
		frmRemotekeysV.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmRemotekeysV.getContentPane().setLayout(null);
		
		ipAddrTxt = new JTextField();
		ipAddrTxt.setText(ipAddr);
		ipAddrTxt.setEditable(false);
		ipAddrTxt.setBounds(112, 44, 132, 20);
		frmRemotekeysV.getContentPane().add(ipAddrTxt);
		ipAddrTxt.setColumns(10);
		
		portTxt = new JTextField();
		portTxt.setText(port);
		portTxt.setEditable(false);
		portTxt.setBounds(344, 44, 96, 20);
		frmRemotekeysV.getContentPane().add(portTxt);
		portTxt.setColumns(10);
		
		JLabel lblYourIpAddress = new JLabel("Your IP address");
		lblYourIpAddress.setBounds(10, 47, 92, 14);
		frmRemotekeysV.getContentPane().add(lblYourIpAddress);
		
		JLabel lblPort = new JLabel("Port");
		lblPort.setBounds(306, 47, 28, 14);
		frmRemotekeysV.getContentPane().add(lblPort);
		
		JTextArea txt_Info = new JTextArea();
		txt_Info.setFont(new Font("Monospaced", Font.PLAIN, 10));
		txt_Info.setEditable(false);
		txt_Info.setBackground(SystemColor.menu);
		txt_Info.setText("Thank you for trying RemoteKeys.\r\n\r\n1.) Please download and install RemoteKeys client app on your phone\r\n\r\n2.) Enter your IP address and port which are provided in this window.\r\n\r\nIf you encounter any bugs turn it off and on again");
		txt_Info.setBounds(10, 101, 464, 182);
		frmRemotekeysV.getContentPane().add(txt_Info);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 484, 22);
		frmRemotekeysV.getContentPane().add(menuBar);
		
		JMenu mnNewMenu = new JMenu("Options");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Change port");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							
							ChangeSocketDialog socketDialog = new ChangeSocketDialog();
							socketDialog.setVisible(true);
							
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenu mnNewMenu_1 = new JMenu("Help");
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("About");
		mnNewMenu_1.add(mntmNewMenuItem_1);
	}
}
